#ifndef ROTAT_IMAGE_H
#define ROTAT_IMAGE_H
#include "stddef.h"
#include "stdint.h"
#include <stdio.h>
#include <stdlib.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    size_t width, height;
    struct pixel *data;
};

enum status {
    READ_OK ,
    OPEN_ERROR,
    TYPE_ERROR,
    READ_ERROR,
    WRITE_ERROR,

};

enum status f_open(const char *file_name, FILE **file, char *mod);

void f_close(FILE *file);


struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
void write_status(const enum status status);

void delete_image(const struct image image);

struct image image_create(const size_t width,const size_t height);

struct image image_create_rot(const size_t width, const size_t height, struct pixel * data );

#endif //ROTAT_IMAGE_H
