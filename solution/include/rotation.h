
#ifndef ROTAT_ROTATION_H
#define ROTAT_ROTATION_H
#include "image.h"

struct image rotate_90(const struct image *image, struct pixel *data);
#endif //ROTAT_ROTATION_H
