#ifndef ROTAT_WRITEBMP_H
#define ROTAT_WRITEBMP_H
#include "image.h"
#include <stdio.h>


enum status to_bmp(FILE *out, struct image const* image);

enum status from_bmp(FILE *in, struct image *image);

#endif //ROTAT_WRITEBMP_H
