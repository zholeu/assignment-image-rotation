#include "../include/image.h"
#include "stddef.h"
#include <stdio.h>
#include <stdlib.h>


char *statusInf[] = {
        [READ_OK] = "Wow, job is done without errors, image is now rotated",
        [OPEN_ERROR] = "Can't open the file",
        [TYPE_ERROR] = "need BMP",
        [READ_ERROR] = "While reading bmp file an error has  occurred",
        [WRITE_ERROR] = "While writing bmp file an error has  occurred",
};

void write_status(const enum status status) {
    fprintf(stdout,"State Info: %s\n", statusInf[status]);
}

struct image image_create(const size_t width, const size_t height) {
    return (struct image) {.width = width, .height = height, .data = malloc(
            width * height * sizeof(struct pixel))};
}

struct image image_create_rot(const size_t width, const size_t height, struct pixel * data ) {
    return (struct image) {.width = width, .height = height, .data =  data};
}

void delete_image(const struct image image) {
    free(image.data);
}

enum status f_open(const char *file_name, FILE **file, char *mod) {

    enum status state = READ_OK;
     *file = fopen(file_name, mod);
    if (*file == NULL) {
        state = OPEN_ERROR;
    }

    return state;
}

void f_close(FILE *file) {
    fclose(file);
}

