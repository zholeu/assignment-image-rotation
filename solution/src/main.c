#include "../include/image.h"
#include "../include/rotation.h"
#include "../include/writeBMP.h"
#include <stdlib.h>

int main(int argc, char **argv){
    if (argc != 3) {
        fprintf(stderr, "2 arguments are required with the bmp format\n");
        exit(1);
    }

    const char *input_name = argv[1];
    const char *output_name = argv[2];

    FILE *input_file = {0};
    FILE *output_file = {0};

    enum status status = f_open(input_name, &input_file, "rb");
    if(status == OPEN_ERROR)
    {
        write_status(status);
        exit(1);
    }


    struct image image = {0};

   status =  from_bmp(input_file,&image);
   if(status == OPEN_ERROR || status== TYPE_ERROR )
   {
       write_status(status);
       exit(1);
   }

     f_close(input_file);

   const struct image sec = rotate_90(&image,image.data);
   delete_image(image);

     status = f_open(output_name, &output_file, "wb");
    if(status == OPEN_ERROR)
    {
        write_status(status);
        exit(1);
    }
   
    status = to_bmp(output_file, &sec);
    if(status == WRITE_ERROR)
    {
        write_status(status);
        exit(1);
    }

     f_close(output_file);

   delete_image(sec);

    write_status(READ_OK);
    
   return 0;
}

