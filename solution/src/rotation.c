#include "../include/rotation.h"

struct image rotate_90(const struct image *image, struct pixel *data) {
    data = malloc(sizeof(struct pixel) * image->width * image->height);

    for (size_t i = 0; i < image->height; ++i) {
        for (size_t j = 0; j < image->width; ++j) {
            data[image->height * j + (image->height - 1 - i)] = image->data[i * image->width + j];
        }
    }

    return image_create_rot(image->height,image->width,data);

}
