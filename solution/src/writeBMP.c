#include "../include/writeBMP.h"
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#define HEADER_SIZE 54

const uint16_t format = 0x4D42;
const uint32_t data_offset = 54;
const uint32_t info_header_size = 40;
const uint16_t planes = 1;
const uint16_t bit_count = 24;

static size_t paddingSize(const size_t width) {
    return width % 4 == 0 ? 0 : 4 - ((width * sizeof(struct pixel)) % 4);
}

enum status from_bmp(FILE *in, struct image *image) {

    enum status status = READ_OK;
    struct bmp_header header = {0};
  

    if(fread(&header, sizeof(struct bmp_header), 1, in)!=1) status = READ_ERROR;

    if (header.bfType != format) {
        fclose(in);
        return TYPE_ERROR;
    }

    const uint32_t bytesPerPixel = ((uint32_t) header.biBitCount) / 8;
    const size_t width  = header.biWidth;
    const size_t height = header.biHeight;
    *image = image_create(width,height);

    const uint32_t paddedRowSize = (((header.biWidth) * (bytesPerPixel) + 3) / 4) * 4;

    for (size_t i = 0; i < image->height; ++i)
    {
        if(fseek(in, (long) (header.bOffBits + (i * paddedRowSize)), SEEK_SET)!=0) status = READ_ERROR;

        for (size_t j = 0; j < image->width; j++) {
            if(fread(&(image->data[image->width * i + j]), sizeof(struct pixel), 1, in)!=1) status = READ_ERROR;
        }
    }

   
    return status;
}


enum status to_bmp(FILE *outputFile, struct image const* image)
{
    enum status status = READ_OK;

    const uint32_t paddedRowSize = (((image->width) * sizeof(struct pixel) + 3) / 4) * 4 ;
    const uint32_t fileSize = paddedRowSize*image->height + HEADER_SIZE ;

     struct bmp_header header = (struct bmp_header) {
            .bfType = format,
            .bfileSize = fileSize,
            .bfReserved = 0,
            .bOffBits = data_offset,
            .biSize = info_header_size,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = planes,
            .biBitCount = bit_count,
            .biCompression = 0,
            .biSizeImage =  image->width*image->height*sizeof(struct pixel),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
   if(fwrite(&header, sizeof(struct bmp_header), 1, outputFile)!=1) status = WRITE_ERROR;
   
 
    if(fseek(outputFile, header.bOffBits, SEEK_SET)!=0) status = WRITE_ERROR;
    const uint8_t zero = 0;

    for (size_t i = 0; i < image->height; i++)
    {
        if(fwrite(image->data + i * image->width, image->width * sizeof(struct pixel), 1, outputFile)!=1) status = WRITE_ERROR;
        for (size_t j = 0; j < paddingSize(image->width); j++) {
            if(fwrite(&zero, 1, 1, outputFile)!=1) status = WRITE_ERROR;
        }
    }
    

    return status;
}
